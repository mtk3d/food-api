package db

import (
	"context"
	"strconv"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"github.com/mtk3d/food-api/internal/food/domain"
)

type DynamoFoodRepository struct {
	dynamo *dynamodb.Client
}

func NewDynamoFoodRepository(dynamo *dynamodb.Client) *DynamoFoodRepository {
	r := new(DynamoFoodRepository)
	r.dynamo = dynamo

	return r
}

func (r *DynamoFoodRepository) SaveFood(food *domain.Food) {
	r.RemoveFood(food.Id)

	_, err := r.dynamo.PutItem(context.TODO(), &dynamodb.PutItemInput{
		TableName: aws.String("food"),
		Item: map[string]types.AttributeValue{
			"id":     &types.AttributeValueMemberN{Value: strconv.Itoa(int(food.Id))},
			"name":   &types.AttributeValueMemberS{Value: food.Name},
			"price":  &types.AttributeValueMemberS{Value: food.Price},
			"weight": &types.AttributeValueMemberN{Value: strconv.Itoa(int(food.Weight))},
		},
	})

	if err != nil {
		panic(err)
	}
}

func (r *DynamoFoodRepository) RemoveFood(id int64) {
	_, err := r.dynamo.DeleteItem(context.TODO(), &dynamodb.DeleteItemInput{
		TableName: aws.String("food"),
		Key: map[string]types.AttributeValue{
			"id": &types.AttributeValueMemberS{Value: strconv.Itoa(int(id))},
		},
	})

	if err != nil {
		panic(err)
	}
}

func (r *DynamoFoodRepository) GetFood() *[]domain.Food {
	out, err := r.dynamo.Scan(context.TODO(), &dynamodb.ScanInput{
		TableName: aws.String("food"),
	})

	if err != nil {
		panic(err)
	}

	var food *[]domain.Food
	err = attributevalue.UnmarshalListOfMaps(out.Items, &food)
	if err != nil {
		panic(err)
	}

	return food
}

func (r *DynamoFoodRepository) FindFood(id int64) (*domain.Food, error) {
	out, err := r.dynamo.GetItem(context.TODO(), &dynamodb.GetItemInput{
		TableName: aws.String("food"),
		Key: map[string]types.AttributeValue{
			"id": &types.AttributeValueMemberS{Value: strconv.Itoa(int(id))},
		},
	})

	if err != nil {
		return nil, err
	}

	var food *domain.Food
	err = attributevalue.UnmarshalMap(out.Item, &food)
	if err != nil {
		panic(err)
	}

	return food, nil
}
