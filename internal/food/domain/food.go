package domain

import "github.com/mtk3d/food-api/internal/food/application"

type Food struct {
	Id     int64
	Name   string
	Price  string
	Weight int64
}

func NewFood(id int64, name string, price string, weight int64) *Food {
	return &Food{
		Id:     id,
		Name:   name,
		Price:  price,
		Weight: weight,
	}
}

func NewFoodFromDTO(dto application.FoodDTO) *Food {
	return NewFood(dto.Id, dto.Name, dto.Price, dto.Weight)
}

func (f *Food) Bite(bite Bite) {
	f.Weight -= bite.GetWeight()
}

func (f *Food) ToDTO() *application.FoodDTO {
	return &application.FoodDTO{
		Id:     f.Id,
		Name:   f.Name,
		Price:  f.Price,
		Weight: f.Weight,
	}
}
