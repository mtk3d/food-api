// @title           food-api
// @version         1.0
// @description     Example food api to learn golang

// @license.name  Apache 2.0
// @license.url   http://www.apache.org/licenses/LICENSE-2.0.html

// @host      localhost:8080
// @BasePath  /
package server

import (
	"context"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/mtk3d/food-api/cmd/flags"
	"github.com/mtk3d/food-api/internal/food/application/getfood"
	"github.com/mtk3d/food-api/internal/food/application/postbite"
	"github.com/mtk3d/food-api/internal/food/application/postfood"
	"github.com/mtk3d/food-api/internal/food/infrastructure/db"
	"github.com/mtk3d/food-api/internal/validator"
	"github.com/urfave/cli/v2"
)

func Command(c *cli.Context) error {
	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{c.String(flags.CorsAllowOrigin)},
		AllowHeaders: []string{"*"},
	}))

	e.Validator = validator.NewValidator()

	cfg, err := config.LoadDefaultConfig(context.TODO(), func(o *config.LoadOptions) error {
		o.Region = "us-east-1"
		return nil
	})
	if err != nil {
		panic(err)
	}
	dynamo := dynamodb.NewFromConfig(cfg)
	repository := db.NewDynamoFoodRepository(dynamo)

	e.GET("/food", getfood.NewGetFood(repository).GetFood)
	e.POST("/food", postfood.NewPostFood(repository).PostFood)
	e.POST("/food/:id/bite", postbite.NewPostBite(repository).PostBite)

	e.Logger.Fatal(e.Start(":8080"))

	return nil
}
